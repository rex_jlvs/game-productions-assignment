using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PickupCrate : MonoBehaviour
{
    public AudioClip pickupSound;
    private AudioSource pickupSource;

    [SerializeField]
    private GameObject PistolP;

    [SerializeField]
    private GameObject ShotgunP;

    [SerializeField]
    private GameObject MachinegunP;

    [SerializeField]
    private GameObject RifleP;

    [SerializeField]
    private GameObject PistolT;

    [SerializeField]
    private GameObject ShotgunT;

    [SerializeField]
    private GameObject MachinegunT;

    [SerializeField]
    private GameObject RifleT;

    void Awake()
    {
        pickupSource = this.gameObject.AddComponent<AudioSource>();
        pickupSource.loop = false;
        pickupSource.playOnAwake = false;
        if (pickupSound != null)
            pickupSource.clip = pickupSound;
    }
    void Start()
    {
        //The actual weapon
        PistolP.SetActive(true);
        ShotgunP.SetActive(false);
        MachinegunP.SetActive(false);
        RifleP.SetActive(false);
        PistolT.SetActive(true);
        ShotgunT.SetActive(false);
        MachinegunT.SetActive(false);
        RifleT.SetActive(false);
    }

    void OnCollisionEnter(Collision collision)
    {

        if (collision.gameObject.tag == "Crate")
        {
            if (pickupSound != null)
            {
                pickupSource.Play();
            }

            Destroy(collision.gameObject);

            if (Random.value <= 0.2) //20 %
            {
                MachinegunT.gameObject.SetActive(true);
                RifleT.gameObject.SetActive(false);
                ShotgunT.gameObject.SetActive(false);
                PistolT.gameObject.SetActive(false);
                MachinegunP.gameObject.SetActive(true);
                RifleP.gameObject.SetActive(false);
                ShotgunP.gameObject.SetActive(false);
                PistolP.gameObject.SetActive(false);
            }
            else if (Random.value <= 0.6) //40 %
            {
                MachinegunT.gameObject.SetActive(false);
                RifleT.gameObject.SetActive(true);
                ShotgunT.gameObject.SetActive(false);
                PistolT.gameObject.SetActive(false);
                MachinegunP.gameObject.SetActive(false);
                RifleP.gameObject.SetActive(true);
                ShotgunP.gameObject.SetActive(false);
                PistolP.gameObject.SetActive(false);

            }
            else if (Random.value <= 1) // 50 %
            {
                MachinegunT.gameObject.SetActive(false);
                RifleT.gameObject.SetActive(false);
                ShotgunT.gameObject.SetActive(true);
                PistolT.gameObject.SetActive(false);
                MachinegunP.gameObject.SetActive(false);
                RifleP.gameObject.SetActive(false);
                ShotgunP.gameObject.SetActive(true);
                PistolP.gameObject.SetActive(false);

            }


        }
    }
}
