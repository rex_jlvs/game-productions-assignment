using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CratePickup2 : MonoBehaviour
{
    [SerializeField]
    private GameObject Pistol;

    [SerializeField]
    private GameObject Shotgun;

    [SerializeField]
    private GameObject Rifle;

    [SerializeField]
    private GameObject Machinegun;

    void Start()
    {
        Pistol.SetActive(false);
        Shotgun.SetActive(false);
        Machinegun.SetActive(false);
        Rifle.SetActive(false);
    }

    void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.tag == "Crate")
        {
            Destroy(collision.gameObject);
            Pistol.SetActive(false);
            Shotgun.SetActive(false);
            Machinegun.SetActive(false);
            Rifle.SetActive(false);

            if (Random.value <= 0.1) //10 %
            {
                Machinegun.gameObject.SetActive(true);
            }
            else if (Random.value <= 0.3) //20 %
            {
                Rifle.gameObject.SetActive(true);
            }
            else if (Random.value <= 0.6) // 30 %
            {
                Shotgun.gameObject.SetActive(true);
            }
            else //40 %
            {
                Pistol.gameObject.SetActive(true);
            }

        }
    }

}
