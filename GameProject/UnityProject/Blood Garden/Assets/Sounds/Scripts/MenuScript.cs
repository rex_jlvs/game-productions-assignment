﻿using System.Collections;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class MenuScript : MonoBehaviour 
{
	void Awake()
	{
		Cursor.lockState = CursorLockMode.Confined;
		Cursor.visible = true; // make the cursor useable again.
	}
	public void PlayGame()
	{
		SceneManager.LoadScene("DialogueScene");
	}

	public void QuitGame()
    {
		Application.Quit();
    }
}
