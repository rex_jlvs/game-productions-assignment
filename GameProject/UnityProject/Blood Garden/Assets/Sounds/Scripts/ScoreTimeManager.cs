using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class ScoreTimeManager : MonoBehaviour
{
    public static ScoreTimeManager instance;
    public Text scoreText;
    public Text highscoreText;

    int score = 0;
    int highscore = 0;

    public float timeValue = 1;
    public Text timerText;

    private void Awake()
    {
        instance = this;
    }
    void Start()
    {
        highscore = PlayerPrefs.GetInt("highscore", 0);
        scoreText.text = score.ToString() + " POINTS";
        highscoreText.text = "HIGHSCORE: " + highscore.ToString();
    }

    void Update()
    {
        if (timeValue > 0)
        {
            timeValue += Time.deltaTime;
        }
        else
        {
            timeValue = 0;
        }

        DisplayTime(timeValue);

    }

    void DisplayTime(float timeToDisplay)
    {
        if (timeToDisplay < 0)
        {
            timeToDisplay = 0;
        }

        float minutes = Mathf.FloorToInt(timeToDisplay / 60);
        float seconds = Mathf.FloorToInt(timeToDisplay % 60);

        timerText.text = string.Format("{0:00}:{1:00}", minutes, seconds);
    }

    public void AddPoint()
    {
        score += 5;
        scoreText.text = score.ToString() + " Points";
        if (highscore < score)
            PlayerPrefs.SetInt("highscore", score);

    }
    public void AddPoint1()
    {
        score += 10;
        scoreText.text = score.ToString() + " Points";
        if (highscore < score)
            PlayerPrefs.SetInt("highscore", score);

    }
    public void AddPoint2()
    {
        score += 20;
        scoreText.text = score.ToString() + " Points";
        if (highscore < score)
            PlayerPrefs.SetInt("highscore", score);

    }

    public void GameOver()
    {
        PlayerPrefs.SetInt("score", score);
        PlayerPrefs.SetInt("time", (int)timeValue);
        SceneManager.LoadScene("GameOverScene");
    }
}
