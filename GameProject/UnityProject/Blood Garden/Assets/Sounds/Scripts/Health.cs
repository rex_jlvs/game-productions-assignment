using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Health : MonoBehaviour
{
	public Image healthBar;
	public float healthAmount = 100;
	public string gameOverScene;

	float invulerabletime = 1;

	private Rigidbody playerRidg;

	void Update()
	{
		if(healthAmount <= 0)
        {
			ScoreTimeManager.instance.GameOver();
		}

		if (invulerabletime > 0)
        {
			invulerabletime -= Time.deltaTime;
        }

	}

	public void OnCollisionEnter(Collision collision)
	{

		if (collision.gameObject.CompareTag("Goblin"))
        {
			if(invulerabletime <= 0)
            {
				TakeDamage(10);
				invulerabletime = 1;
			}
        }

		if (collision.gameObject.CompareTag("Slime"))
		{
			if (invulerabletime <= 0)
			{
				TakeDamage(20);
				invulerabletime = 1;
			}
		}

		if (collision.gameObject.CompareTag("SmallSlime"))
		{
			if (invulerabletime <= 0)
			{
				TakeDamage(10);
				invulerabletime = 1;
			}
		}

		if (collision.gameObject.CompareTag("Gnome"))
		{
			if (invulerabletime <= 0)
			{
				TakeDamage(30);
				invulerabletime = 1;
			}
		}
	}

	public void TakeDamage(float Damage)
	{
		healthAmount -= Damage;
		healthBar.fillAmount = healthAmount / 100;
	}
}
