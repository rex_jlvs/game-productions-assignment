using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Deathsound : MonoBehaviour
{
    public AudioClip deathSound;
    private AudioSource deathSource;

    void Awake()
    {
        deathSource = this.gameObject.AddComponent<AudioSource>();
        deathSource.loop = false;
        deathSource.playOnAwake = false;
        if (deathSound != null)
            deathSource.clip = deathSound;
    }

    void Start()
    {
        if (deathSound != null)
        {
            deathSource.Play();
        }
    }
}
