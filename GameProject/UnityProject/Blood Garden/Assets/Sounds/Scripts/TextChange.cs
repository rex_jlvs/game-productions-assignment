using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TextChange : MonoBehaviour
{
    public GameObject ActiveText;
    public GameObject DisableText;
    public GameObject DisableText2;
    public GameObject DisableText3;

    public void Transformtext ()
    {
        ActiveText.gameObject.SetActive(true);
        DisableText.gameObject.SetActive(false);
        DisableText2.gameObject.SetActive(false);
        DisableText3.gameObject.SetActive(false);
    }
}
