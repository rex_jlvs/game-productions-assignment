using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CratePickUp : MonoBehaviour
{
    [SerializeField]
    private GameObject Pistol;

    [SerializeField]
    private GameObject Shotgun;

    [SerializeField]
    private GameObject Rifle;

    [SerializeField]
    private GameObject Machinegun;

    [SerializeField]
    private GameObject Pistol2;

    [SerializeField]
    private GameObject Shotgun2;

    [SerializeField]
    private GameObject Rifle2;

    [SerializeField]
    private GameObject Machinegun2;

    [SerializeField]
    private GameObject Pistol3;

    [SerializeField]
    private GameObject Shotgun3;

    [SerializeField]
    private GameObject Rifle3;

    [SerializeField]
    private GameObject Machinegun3;

    [SerializeField]
    private GameObject PistolP;

    [SerializeField]
    private GameObject ShotgunP;

    [SerializeField]
    private GameObject MachinegunP;

    [SerializeField]
    private GameObject RifleP;

    private float PistolChance;
    private float ShotgunChance;
    private float RifleChance;
    private float MachineGunChance;



    void Start()
    {
        //Weapon buttons
        Pistol.SetActive(false);
        Shotgun.SetActive(false);
        Machinegun.SetActive(false);
        Rifle.SetActive(false);
        Pistol2.SetActive(false);
        Shotgun2.SetActive(false);
        Machinegun2.SetActive(false);
        Rifle2.SetActive(false);
        Pistol3.SetActive(false);
        Shotgun3.SetActive(false);
        Machinegun3.SetActive(false);
        Rifle3.SetActive(false);

        //The actual weapon
        PistolP.SetActive(true);
        ShotgunP.SetActive(false);
        MachinegunP.SetActive(false);
        RifleP.SetActive(false);
    }

    void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.tag == "Crate")
        {
            Destroy(collision.gameObject);
            Pistol.SetActive(false);
            Shotgun.SetActive(false);
            Machinegun.SetActive(false);
            Rifle.SetActive(false);
            Pistol2.SetActive(false);
            Shotgun2.SetActive(false);
            Machinegun2.SetActive(false);
            Rifle2.SetActive(false);
            Pistol3.SetActive(false);
            Shotgun3.SetActive(false);
            Machinegun3.SetActive(false);
            Rifle3.SetActive(false);


            MachineGunChance = 0.1f;
            RifleChance = 0.3f;
            ShotgunChance = 0.6f;
            PistolChance = 1.0f;




            if (Random.value <= MachineGunChance) //10 %
            {
                Machinegun.gameObject.SetActive(true);
                MachineGunChance = 0;
            }
            else if (Random.value <= RifleChance) //20 %
            {
                Rifle.gameObject.SetActive(true);
                RifleChance = 0;
            }
            else if (Random.value <= ShotgunChance) // 30 %
            {
                Shotgun.gameObject.SetActive(true);
                ShotgunChance = 0;
            }
            else if (Random.value <= PistolChance)
            {
                Pistol.gameObject.SetActive(true);
                PistolChance = 0;
            }

            if (Random.value <= MachineGunChance) //10 %
            {
                Machinegun2.gameObject.SetActive(true);
                MachineGunChance = 0;
            }
            else if (Random.value <= RifleChance) //20 %
            {
                Rifle2.gameObject.SetActive(true);
                RifleChance = 0;
            }
            else if (Random.value <= ShotgunChance) // 30 %
            {
                Shotgun2.gameObject.SetActive(true);
                ShotgunChance = 0;
            }
            else if (Random.value <= PistolChance)
            {
                Pistol2.gameObject.SetActive(true);
                PistolChance = 0;
            }

            if (Random.value <= MachineGunChance) //10 %
            {
                Machinegun3.gameObject.SetActive(true);
                MachineGunChance = 0;
            }
            else if (Random.value <= RifleChance) //20 %
            {
                Rifle3.gameObject.SetActive(true);
                RifleChance = 0;
            }
            else if (Random.value <= ShotgunChance) // 30 %
            {
                Shotgun3.gameObject.SetActive(true);
                ShotgunChance = 0;
            }
            else if (Random.value <= PistolChance)
            {
                Pistol3.gameObject.SetActive(true);
                PistolChance = 0;
            }


        }
    }

}
