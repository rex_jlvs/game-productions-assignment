using System.Collections;
using System.Collections.Generic;
using UnityEngine;
[RequireComponent(typeof(Rigidbody))]

public class Bullet : MonoBehaviour
{
    public float m_Speed = 10f;
    public float m_Lifespan = 3f;


    private Rigidbody m_Rigidbody;

    public AudioClip collisionSound;
    private AudioSource collisionSource;
    public GameObject collisionParticle;
    private GameObject sourceObj;
    public string tagName;


	void Awake()
	{
		m_Rigidbody = GetComponent<Rigidbody>();
		sourceObj = new GameObject();
		sourceObj.transform.parent = this.transform;
		collisionSource = sourceObj.AddComponent<AudioSource>();
		collisionSource.loop = false;
		collisionSource.playOnAwake = false;
		if (collisionSound != null)
			collisionSource.clip = collisionSound;
	}

	void Start()
	{
		m_Rigidbody.AddForce(m_Rigidbody.transform.forward * m_Speed);
		Destroy(gameObject, m_Lifespan);
	}

	void OnCollisionEnter(Collision col)
	{
		Debug.Log(col.gameObject.tag);
		if (col.gameObject.tag != "Bullet")
		{
			DoStuff(col.gameObject);
		}
	}

	void OnTriggerEnter(Collider col)
	{
		if (col.gameObject.tag != "Bullet")
		{
			DoStuff(col.gameObject);
		}
	}

	void DoStuff(GameObject col)
	{
		if (collisionSound != null)
		{
			collisionSource.Play();
			sourceObj.transform.parent = null;
			Destroy(sourceObj, collisionSound.length + 1f);
		}
		if (collisionParticle != null)
		{
			GameObject temp = (GameObject)Instantiate(collisionParticle, transform.position, Quaternion.identity);
			ParticleSystem ps = temp.GetComponent<ParticleSystem>();
			ps.Play();
			Destroy(temp, ps.main.duration + 1f);
		}
		if (col.tag == tagName)
		{
			Destroy((Object)col);
		}
		Destroy((Object)this.gameObject);
	}
}
