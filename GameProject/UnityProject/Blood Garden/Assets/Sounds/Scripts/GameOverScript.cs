﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class GameOverScript : MonoBehaviour 
{
	public Text scoretx;
	public Text highscoretx;
	public Text timetx;

	void Awake()
	{
		Cursor.lockState = CursorLockMode.Confined;
		Cursor.visible = true; // make the cursor useable again.
		scoretx.text = "Final Score: " + PlayerPrefs.GetInt("score").ToString();
		highscoretx.text = "Highscore: " + PlayerPrefs.GetInt("highscore").ToString();
		timetx.text = PlayerPrefs.GetInt("time").ToString() + " Seconds";
	}
	public void RestartGame()
	{
		SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex * 0);
	}

	public void QuitGame()
	{
		Application.Quit();
	}
}
