using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Topdown : MonoBehaviour
{
    private InputHandler _input;

    [SerializeField]
    private float moveSpeed;
    [SerializeField]
    private float rotatespeed;

    [SerializeField]
    private Camera camera;

    [SerializeField]
    private bool rotateTowardsMouse;

    private void Awake()
    {
        _input = GetComponent<InputHandler>();
    }

    // Update is called once per frame
    void Update()
    {
        var targetVector = new Vector3(_input.Inputvector.x, 0, _input.Inputvector.y);

        //Move in the direction we are aiming
        var movementVector = MoveTowardTarget(targetVector);
        if (rotateTowardsMouse)
            // Rotate in the direction we are travelling
            RotatetowardMovementVector(movementVector);
        else
            RotatetowardMouseVector();

    }

    private void RotatetowardMouseVector()
    {
        Ray ray = camera.ScreenPointToRay(_input.Mouseposition);

        if(Physics.Raycast(ray, out RaycastHit hitInfo, maxDistance: 300f))
        {
            var target = hitInfo.point;
            target.y = transform.position.y;
            transform.LookAt(target);
        }
    }

    private void RotatetowardMovementVector(Vector3 movementVector)
    {
        if(movementVector.magnitude == 0) { return; }
        var rotation = Quaternion.LookRotation(movementVector);
        transform.rotation = Quaternion.RotateTowards(transform.rotation, rotation, rotatespeed);
    }

    private Vector3 MoveTowardTarget(Vector3 targetVector)
    {
        var speed = moveSpeed * Time.deltaTime;

        //Important: Player movement despite camera position. Remeber the rotation
        targetVector = Quaternion.Euler(0, camera.gameObject.transform.eulerAngles.y, 0) * targetVector;
        var targetposition = transform.position + targetVector * speed;
        transform.position = targetposition;
        return targetVector;
    }
}
