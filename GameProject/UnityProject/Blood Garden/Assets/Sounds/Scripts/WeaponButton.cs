using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class WeaponButton : MonoBehaviour
{
    public GameObject Guntype;
    public GameObject disable1;
    public GameObject disable2;
    public GameObject disable3;


    public void Activation()
    {
        Guntype.gameObject.SetActive(true);
        disable1.gameObject.SetActive(false);
        disable2.gameObject.SetActive(false);
        disable3.gameObject.SetActive(false);
    }
}
