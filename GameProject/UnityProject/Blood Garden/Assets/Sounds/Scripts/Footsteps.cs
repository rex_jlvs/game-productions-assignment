using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Footsteps : MonoBehaviour
{
    public AudioClip footstep;
    private AudioSource footSource;
    Vector3 lastPos;
    public GameObject self;


    void Awake()
    {
        footSource = this.gameObject.AddComponent<AudioSource>();
        footSource.loop = false;
        footSource.playOnAwake = false;
        if (footstep != null)
            footSource.clip = footstep;
    }

    void Update()
    {
        if (self.transform.position != lastPos)
        {
            if (footstep != null)
            {
                footSource.Play();
            }
        }
        else
        {
            footSource.Stop();
        }

        lastPos = self.transform.position;
    }
}
