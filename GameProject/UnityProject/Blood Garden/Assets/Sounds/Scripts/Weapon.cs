
using UnityEngine;
using TMPro;

public class Weapon : MonoBehaviour
{
    public GameObject CurrentWeapon;
    public GameObject CurrentWeaponT;
    public GameObject Switchback;
    public GameObject SwitchbackT;
    public GameObject m_Projectile;    
    public Transform m_SpawnTransform;

    public AudioClip shootSound;
    private AudioSource shootSource;
    public GameObject shootParticle;
    private ParticleSystem shootSystem;

    public float timebtwShooting, spread, timebtwShots;
    public int magazineSize, bulletsPetTap;
    public bool allowButtonHold;
    int bulletsLeft, bulletsShot;

    bool shooting, readyToShoot;

    public TextMeshProUGUI text;


    void Awake()
    {
        bulletsLeft = magazineSize;
        readyToShoot = true;

        shootSource = this.gameObject.AddComponent<AudioSource>();
        shootSource.loop = false;
        shootSource.playOnAwake = false;
        if (shootSound != null)
            shootSource.clip = shootSound;

    }

    private void Update()
    {

        Myinput();

        //SetText
        text.SetText(bulletsLeft + " / " + magazineSize);

    }
    private void Myinput()
    {
        if (allowButtonHold) shooting = Input.GetKey(KeyCode.Mouse0);
        else shooting = Input.GetKeyDown(KeyCode.Mouse0);

        if (bulletsLeft <= 0) Break();

        //Shoot
        if (readyToShoot && shooting && bulletsLeft > 0)
        {
            bulletsShot = bulletsPetTap;
            Shoot();
        }

    }

    private void Break()
    {
        FindObjectOfType<Switchsound>().SoundTrigger();
        bulletsLeft = magazineSize;
        CurrentWeapon.gameObject.SetActive(false);
        CurrentWeaponT.gameObject.SetActive(false);
        Switchback.gameObject.SetActive(true);
        SwitchbackT.gameObject.SetActive(true);


    }

    private void Shoot()
    {
        readyToShoot = false;

        GameObject bullet = Instantiate(m_Projectile, m_SpawnTransform.position, m_SpawnTransform.rotation);
        bullet.transform.Rotate(0, Random.Range(-spread, spread), 0);
            if (shootParticle != null)
            {
                shootSystem = ((GameObject)Instantiate(shootParticle, m_SpawnTransform.position, m_SpawnTransform.rotation)).GetComponent<ParticleSystem>();
                Destroy((GameObject)shootSystem.gameObject, shootSystem.main.duration);
            }
            if (shootSound != null)
                shootSource.Play();

        bulletsLeft--;
        bulletsShot--;

        Invoke("ResetShot", timebtwShooting);

        if (bulletsShot > 0 && bulletsLeft > 0)
        Invoke("Shoot", timebtwShots);
    }

    private void ResetShot()
    {
        readyToShoot = true;
    }


}
