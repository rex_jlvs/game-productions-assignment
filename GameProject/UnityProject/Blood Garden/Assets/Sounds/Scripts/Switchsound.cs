using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Switchsound : MonoBehaviour
{
    public AudioClip switchSound;
    private AudioSource switchSource;

    void Awake()
    {
        switchSource = this.gameObject.AddComponent<AudioSource>();
        switchSource.loop = false;
        switchSource.playOnAwake = false;
        if (switchSound != null)
            switchSource.clip = switchSound;
    }

    public void SoundTrigger()
    {
        if (switchSound != null)
        {
            switchSource.Play();
        }
    }
}
