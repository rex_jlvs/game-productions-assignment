using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Enemy : MonoBehaviour
{

    private GameObject player;
    public int MoveSpeed = 4;
    public int MaxDist = 1;
    public int MinDist = 0;


    public Transform SpawnPt;
    public GameObject Drop1;

    public float healthAmount = 100;

    void Start()
    {
        player = GameObject.FindWithTag("Player");
    }

    void Update()
    {
        transform.LookAt(player.transform);

        if (Vector3.Distance(transform.position, player.transform.position) >= MinDist)
        {

            transform.position += transform.forward * MoveSpeed * Time.deltaTime;



            if (Vector3.Distance(transform.position, player.transform.position) <= MaxDist)
            {
               
            }

        }

        if (healthAmount <= 0)
        {

            Death();
        }
    }

    void OnCollisionEnter(Collision collision)
    {

        if (collision.gameObject.CompareTag("Bullet"))
        {
            TakeDam(25);
        }

        if (collision.gameObject.CompareTag("Shotgunbullet"))
        {
            TakeDam(50);
        }

    }

    private void TakeDam(float Damage)
    {
        healthAmount -= Damage;
    }

    private void Death()
    {
        ScoreTimeManager.instance.AddPoint();
        Destroy(this.gameObject);
        Instantiate(Drop1, SpawnPt.position, SpawnPt.rotation);
    }
}
