using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ButtonCancel : MonoBehaviour
{
    public GameObject Cancel1;
    public GameObject Cancel2;
    public GameObject Cancel3;
    public GameObject Cancel4;
    public GameObject Cancel5;
    public GameObject Cancel6;
    public GameObject Cancel7;
    public GameObject Cancel8;
    public GameObject Cancel9;
    public GameObject Cancel10;
    public GameObject Cancel11;
    public GameObject Cancel12;

    public void Cancellation()
    {
        Cancel1.gameObject.SetActive(false);
        Cancel2.gameObject.SetActive(false);
        Cancel3.gameObject.SetActive(false);
        Cancel4.gameObject.SetActive(false);
        Cancel5.gameObject.SetActive(false);
        Cancel6.gameObject.SetActive(false);
        Cancel7.gameObject.SetActive(false);
        Cancel8.gameObject.SetActive(false);
        Cancel9.gameObject.SetActive(false);
        Cancel10.gameObject.SetActive(false);
        Cancel11.gameObject.SetActive(false);
        Cancel12.gameObject.SetActive(false);
    }
}
