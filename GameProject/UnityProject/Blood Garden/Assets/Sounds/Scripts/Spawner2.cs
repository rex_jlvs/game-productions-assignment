using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Spawner2 : MonoBehaviour
{
    public GameObject Enemy1;
    public GameObject Enemy2;
    public GameObject Enemy3;
    public int xPos;
    public int zPos;
    public int xPos2;
    public int zPos2;
    public int xPos3;
    public int zPos3;
    public int slimecount;
    public int gnomecount;
    public int goblincount;

    void Start()
    {
        StartCoroutine(Enemyspawn());
        StartCoroutine(Enemyspawn2());
        StartCoroutine(Enemyspawn3());
    }

    IEnumerator Enemyspawn()
    {
        while (slimecount < 100)
        {
            xPos = Random.Range(1, 200);
            zPos = Random.Range(1, 200);
            Instantiate(Enemy1, new Vector3(xPos, 1, zPos), Quaternion.identity);
            yield return new WaitForSeconds(20f);
            slimecount += 1;
        }
    }
    IEnumerator Enemyspawn2()
    {
        while (gnomecount < 100)
        {
            xPos2 = Random.Range(1, 200);
            zPos2 = Random.Range(1, 200);
            Instantiate(Enemy2, new Vector3(xPos2, 1, zPos2), Quaternion.identity);
            yield return new WaitForSeconds(50f);
            gnomecount += 1;
        }
    }

    IEnumerator Enemyspawn3()
    {
        while (goblincount < 100)
        {
            xPos3 = Random.Range(1, 200);
            zPos3 = Random.Range(1, 200);
            Instantiate(Enemy3, new Vector3(xPos3, 1, zPos3), Quaternion.identity);
            yield return new WaitForSeconds(3f);
            goblincount += 1;
        }
    }
}
