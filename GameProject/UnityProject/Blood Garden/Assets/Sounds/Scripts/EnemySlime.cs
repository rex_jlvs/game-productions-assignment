using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemySlime : MonoBehaviour
{

    private GameObject player;
    public int MoveSpeed = 4;
    public int MaxDist = 1;
    public int MinDist = 0;

    public Transform SpawnPoint;
    public Transform SpawnPoint2;
    public GameObject Prefab;

    public float healthAmount = 200;


    void Start()
    {
        player = GameObject.FindWithTag("Player");
    }

    void Update()
    {
        transform.LookAt(player.transform);

        if (Vector3.Distance(transform.position, player.transform.position) >= MinDist)
        {

            transform.position += transform.forward * MoveSpeed * Time.deltaTime;



            if (Vector3.Distance(transform.position, player.transform.position) <= MaxDist)
            {

            }

        }

        if (healthAmount <= 0)
        {

            Die();
        }
    }

    void OnCollisionEnter(Collision collision)
    {


        if (collision.gameObject.CompareTag("Bullet"))
        {
            TakeDm(25);
        }

        if (collision.gameObject.CompareTag("Player"))
        {

        }

        if (collision.gameObject.CompareTag("Shotgunbullet"))
        {
            TakeDm(50);
        }

    }

    private void TakeDm(float Damage)
    {
        healthAmount -= Damage;
    }

    private void Die()
    {

        ScoreTimeManager.instance.AddPoint1();
        Destroy(this.gameObject);
        Instantiate(Prefab, SpawnPoint.position, SpawnPoint.rotation);
        Instantiate(Prefab, SpawnPoint2.position, SpawnPoint.rotation);

    }
}
