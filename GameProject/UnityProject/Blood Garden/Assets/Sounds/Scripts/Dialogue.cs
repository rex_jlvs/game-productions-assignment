using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class Dialogue : MonoBehaviour
{
    private int clickCount;
    int layerMask = 1 << 8;

    public GameObject PlayerImage;
    public GameObject MonsterImage;

    public GameObject dialogue1;
    public GameObject dialogue2;
    public GameObject dialogue3;
    public GameObject dialogue4;
    public GameObject dialogue5;

    void Start()
    {
        dialogue1.gameObject.SetActive(true);
        dialogue2.gameObject.SetActive(false);
        dialogue3.gameObject.SetActive(false);
        dialogue4.gameObject.SetActive(false);
        dialogue5.gameObject.SetActive(false);
        PlayerImage.gameObject.SetActive(true);
        MonsterImage.gameObject.SetActive(false);
    }
    void Update()
    {
        //Mouseclick
        if (Input.GetMouseButtonDown(0))
        {
            clickCount++;
            if (clickCount == 2)
            {
                dialogue1.gameObject.SetActive(false);
                dialogue2.gameObject.SetActive(true);
                dialogue3.gameObject.SetActive(false);
                dialogue4.gameObject.SetActive(false);
                dialogue5.gameObject.SetActive(false);
                PlayerImage.gameObject.SetActive(true);
                MonsterImage.gameObject.SetActive(false);
            }

            if (clickCount == 3)
            {
                dialogue1.gameObject.SetActive(false);
                dialogue2.gameObject.SetActive(false);
                dialogue3.gameObject.SetActive(true);
                dialogue4.gameObject.SetActive(false);
                dialogue5.gameObject.SetActive(false);
                PlayerImage.gameObject.SetActive(true);
                MonsterImage.gameObject.SetActive(false);
            }

            if (clickCount == 4)
            {
                dialogue1.gameObject.SetActive(false);
                dialogue2.gameObject.SetActive(false);
                dialogue3.gameObject.SetActive(false);
                dialogue4.gameObject.SetActive(true);
                dialogue5.gameObject.SetActive(false);
                PlayerImage.gameObject.SetActive(false);
                MonsterImage.gameObject.SetActive(true);
            }

            if (clickCount == 5)
            {
                dialogue1.gameObject.SetActive(false);
                dialogue2.gameObject.SetActive(false);
                dialogue3.gameObject.SetActive(false);
                dialogue4.gameObject.SetActive(false);
                dialogue5.gameObject.SetActive(true);
                PlayerImage.gameObject.SetActive(true);
                MonsterImage.gameObject.SetActive(false);
            }

            if (clickCount == 6)
            {
                SceneManager.LoadScene("BloodGardenLevel");
            }
        }
    }
}



