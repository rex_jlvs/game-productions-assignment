using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Hurtbox : MonoBehaviour
{
    public GameObject healthbr;
    private Health hpbar;

    void Start()
    {
        hpbar = healthbr.GetComponent<Health>();
    }

    public void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.CompareTag("Player"))
        {
            Debug.Log(hpbar);
            if (hpbar != null)
            {
                hpbar.TakeDamage(30);
            }

        }
    }
}
